#pragma once

#include <filesystem>
#include <string>
#include <vector>

namespace RPGM {
	struct RGSSArchiveEntry {
		std::filesystem::path name;
		std::size_t size;
		std::size_t offset;
		unsigned key;
	};

	class RGSSArchive {
	public:
		void extract(const std::filesystem::path& outputDir, bool overwrite);

	protected:
		RGSSArchive(const std::filesystem::path& path);

		int readVersion();
		virtual std::vector<RGSSArchiveEntry> readEntries(const std::filesystem::path& path) const = 0;

	private:
		static void decryptFileData(std::vector<unsigned char>& data, unsigned keyValue);
		std::filesystem::path path_;
	};

	class RGSSArchiveV1: public RGSSArchive {
	public:
		RGSSArchiveV1(const std::filesystem::path& path);

	protected:
		std::vector<RGSSArchiveEntry> readEntries(const std::filesystem::path& path) const override;
	};

	class RGSSArchiveV3: public RGSSArchive {
	public:
		RGSSArchiveV3(const std::filesystem::path& path);

	protected:
		std::vector<RGSSArchiveEntry> readEntries(const std::filesystem::path& path) const override;
	};
} // namespace RPGM
