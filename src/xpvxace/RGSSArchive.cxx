#include "RGSSArchive.hxx"

#include <cstdint>
#include <cstdio>
#include <fstream>
#include <iostream>
#include <set>
#include <string>

namespace fs = std::filesystem;

namespace {

	const std::string RGSSADHeader = "RGSSAD";
	const int RGASSDv1 = 1;
	const int RGASSDv3 = 3;

	const std::set<int> supportedRGSSVersions = {RGASSDv1, RGASSDv3};

	unsigned RGASSADv1Key = 0xDEADCAFE;

	using Buffer = std::vector<unsigned char>;

	std::string readCString(std::istream& is, std::size_t maxLength) {
		std::string res;
		for (std::size_t i = 0; i < maxLength; ++i) {
			char c = is.get();
			if (c == '\0') { return res; }
			res.push_back(c);
		}
		return res;
	}

	void close_file(std::FILE* fp) {
		std::fclose(fp);
	}

	void writeAll(const fs::path& filename, const Buffer& data) {
		std::unique_ptr<std::FILE, decltype(&close_file)> fp(std::fopen(filename.c_str(), "wb"), &close_file);
		if (!fp) { throw std::runtime_error("Can not open destination file"); }
		if (::fwrite(data.data(), 1, data.size(), fp.get()) != data.size()) {
			throw std::runtime_error("Can't write to file");
		}
	}

	fs::path bufToPath(const Buffer& data) {
		fs::path res;
		std::string cur;
		for (std::size_t i = 0; i < data.size(); ++i) {
			if (data[i] == '\\') {
				if (res.empty()) {
					res = cur;
				} else {
					res /= cur;
				}
				cur.clear();
			} else {
				cur.push_back(data[i]);
			}
		}
		if (res.empty()) {
			res = cur;
		} else {
			res /= cur;
		}
		return res;
	}

} // namespace

RPGM::RGSSArchive::RGSSArchive(const std::filesystem::path& path)
    : path_{path} {
}

int RPGM::RGSSArchive::readVersion() {
	std::ifstream inp{path_};
	std::string header = readCString(inp, 7);
	if (header != RGSSADHeader) { throw std::runtime_error("Not an RSSAD archive"); }
	auto ver = inp.get();
	return supportedRGSSVersions.count(ver) ? ver : -1;
}

void RPGM::RGSSArchive::extract(const std::filesystem::path& outputDir, bool overwrite) {
	const auto entries = readEntries(path_);
	std::unique_ptr<std::FILE, decltype(&close_file)> fp(std::fopen(path_.c_str(), "rb"), &close_file);
	if (!fp) { throw std::runtime_error("Can not open archive file"); }
	for (const RGSSArchiveEntry& e: entries) {
		std::fseek(fp.get(), e.offset, SEEK_SET);
		std::vector<unsigned char> buf(e.size);
		if (::fread(buf.data(), 1, buf.size(), fp.get()) != buf.size()) {
			throw std::runtime_error("Can't read from file");
		}
		decryptFileData(buf, e.key);
		fs::path res = outputDir / e.name;
		if (overwrite || !fs::exists(res)) {
			fs::create_directories(res.parent_path());
			writeAll(res, buf);
		}
	}
}

void RPGM::RGSSArchive::decryptFileData(std::vector<unsigned char>& data, unsigned int keyValue) {
	union {
		std::uint32_t num;
		unsigned char bytes[4];
	} key;

	key.num = keyValue;
	int j = 0;
	for (std::size_t i = 0; i < data.size(); ++i) {
		if (j == 4) {
			j = 0;
			key.num = key.num * 7 + 3;
		}
		data[i] = (data[i] ^ key.bytes[j]);
		++j;
	}
}

RPGM::RGSSArchiveV1::RGSSArchiveV1(const std::filesystem::path& path)
    : RGSSArchive(path) {
	if (readVersion() != RGASSDv1) { throw std::runtime_error("Invalid archive version"); }
}

RPGM::RGSSArchiveV3::RGSSArchiveV3(const std::filesystem::path& path)
    : RGSSArchive(path) {
	if (readVersion() != RGASSDv3) { throw std::runtime_error("Invalid archive version"); }
}

namespace {
	std::uint32_t readInt(::FILE* fp) {
		std::uint32_t tmpInt;
		if (::fread(&tmpInt, 1, 4, fp) != 4) { throw std::runtime_error("Can't read from file"); }
		return tmpInt;
	}

	std::vector<unsigned char>& readInBuf(::FILE* fp, std::vector<unsigned char>& buf, std::uint32_t len) {
		buf.resize(len);
		if (::fread(buf.data(), 1, len, fp) != len) { throw std::runtime_error("Can't read from file"); }
		return buf;
	}
} // namespace

std::vector<RPGM::RGSSArchiveEntry> RPGM::RGSSArchiveV1::readEntries(const std::filesystem::path& path) const {
	unsigned key = RGASSADv1Key;

	std::vector<RGSSArchiveEntry> res;
	std::unique_ptr<std::FILE, decltype(&close_file)> fp(std::fopen(path.c_str(), "rb"), &close_file);
	if (!fp) { throw std::runtime_error("Can not open archive file"); }
	::fseek(fp.get(), 8, SEEK_SET);

	Buffer buf;

	const auto readBuf = [&](std::uint32_t len) -> Buffer& { return readInBuf(fp.get(), buf, len); };

	const auto decryptInt = [&](std::uint32_t val) {
		std::uint32_t res = val ^ key;
		key = key * 7 + 3;
		return res;
	};

	const auto decryptPath = [&](Buffer& buf) {
		for (std::size_t i = 0; i < buf.size(); ++i) {
			buf[i] = buf[i] ^ (key & 0xff);
			key = key * 7 + 3;
		}
		return bufToPath(buf);
	};


	const auto fileSize = fs::file_size(path);
	while (true) {
		RGSSArchiveEntry e;
		auto length = decryptInt(readInt(fp.get()));
		e.name = decryptPath(readBuf(length));
		e.size = decryptInt(readInt(fp.get()));
		e.offset = ::ftell(fp.get());
		e.key = key;
		res.push_back(std::move(e));
		::fseek(fp.get(), e.size, SEEK_CUR);
		if (::ftell(fp.get()) >= fileSize) { break; }
	}
	return res;
}

std::vector<RPGM::RGSSArchiveEntry> RPGM::RGSSArchiveV3::readEntries(const std::filesystem::path& path) const {
	std::unique_ptr<std::FILE, decltype(&close_file)> fp(std::fopen(path.c_str(), "rb"), &close_file);
	if (!fp) { throw std::runtime_error("Can not open archive file"); }
	::fseek(fp.get(), 8, SEEK_SET);

	std::uint32_t key = readInt(fp.get());
	key = key * 9 + 3;

	Buffer buf;

	const auto readBuf = [&](std::uint32_t len) -> Buffer& { return readInBuf(fp.get(), buf, len); };
	const auto decryptInt = [key](std::uint32_t val) { return val ^ key; };

	const auto decryptPath = [&](Buffer& buf) {
		union {
			std::uint32_t num;
			unsigned char bytes[4];
		} keyLocal;

		keyLocal.num = key;

		for (std::size_t i = 0; i < buf.size(); ++i) { buf[i] = buf[i] ^ keyLocal.bytes[i % 4]; }

		// TODO read as UTF-8 on Windows
		// subdirs?
		return bufToPath(buf);
	};

	std::vector<RGSSArchiveEntry> res;

	while (true) {
		RGSSArchiveEntry e;
		e.offset = decryptInt(readInt(fp.get()));
		e.size = decryptInt(readInt(fp.get()));
		e.key = decryptInt(readInt(fp.get()));
		int length = decryptInt(readInt(fp.get()));

		if (e.offset == 0) { break; }

		e.name = decryptPath(readBuf(length));

		res.push_back(std::move(e));
	}

	return res;
}
