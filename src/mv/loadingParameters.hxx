#pragma once

#include <string>

namespace MV {
	struct LoadingParameters {
		LoadingParameters();

		std::string encryptionKeyName;
		std::size_t headerLength;
		std::string signature;
		std::string version;
		std::string remain;
		bool ignoreFakeHeader;
	};
} // namespace MV
