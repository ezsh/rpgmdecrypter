#include "loadingParameters.hxx"

MV::LoadingParameters::LoadingParameters()
    : encryptionKeyName{"encryptionKey"}
    , headerLength{16}
    , signature{"5250474d56000000"}
    , version{"000301"}
    , remain{"0000000000"}
    , ignoreFakeHeader{false} {
}
