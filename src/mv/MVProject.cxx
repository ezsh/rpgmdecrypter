#include "MVProject.hxx"

#include "MVDecrypter.hxx"

#include <boost/algorithm/string/case_conv.hpp>

#include <algorithm>
#include <stdexcept>
#include <utility>

namespace fs = std::filesystem;

namespace {
	std::string defaultEncryptionKey = "encryptionKey";

	fs::path findSystemFile(const fs::path& projectDir) {
		const std::vector<fs::path> options = {// known locations for System.json
		                                       fs::path("data") / "System.json",
		                                       fs::path("www") / "data" / "System.json"};

		for (const auto& o: options) {
			fs::path test = projectDir / o;
			if (fs::exists(test)) { return test; }
		}

		return {};
	}
} // namespace


MV::Project::Project(const LoadingParameters& params, const std::filesystem::path& path, bool verify)
    : projectParams_{params}
    , path_{path}
    , encryptionKeyName_{defaultEncryptionKey}
    , isEncrypted_{true} {
	if (!fs::is_directory(path)) { throw std::runtime_error("Project-Path doesn't exists!"); }

	// Check if Path is a Valid-RPG-Maker-Dir
	if (verify) {
		if (!verifyProject()) { throw std::runtime_error("Directory is not a Valid RPG-Maker-MV Directory!"); }
	}

	system_ = findSystemFile(path_);

	if (!system_.empty()) { ensureEncrypted(); }
}

bool MV::Project::verifyProject() {
	const std::vector<std::string> needles = {// one of these files has to be found for the verification to pass
	                                          "Game.exe", // Mostly with an other name...
	                                          "Game.rpgproject", // Mostly not in the Directory
	                                          "d3dcompiler_47.dll", "ffmpegsumo.dll", "icudtl.dat",   "libEGL.dll",
	                                          "libGLESv2.dll",      "nw.pak",         "package.json", "pdf.dll"};

	return std::any_of(needles.begin(), needles.end(), [this](const std::string& n) { return fs::exists(path_ / n); });
}

void MV::Project::ensureEncrypted() {
	try {
		isEncrypted_ = Decrypter{projectParams_, system_, encryptionKeyName_}.isProjectEncrypted();
	} catch (std::runtime_error*& e) { isEncrypted_ = false; }
#if 0
		if(d.getDecryptCode() != null)
			this.setEncrypted(true);
		else {
			// Test default names
			String decryptKey = Finder.testEncryptionKeyNames(this.getSystem());

			if(decryptKey != null) {
				this.setEncryptionKeyName(decryptKey);
				this.setEncrypted(true);
			}
		}
#endif
}

void MV::Project::decrypt(const std::filesystem::path& outputDir) {
	const auto isEncrypted = [](const fs::path& p) {
		std::string ext = p.extension();

		boost::algorithm::to_lower(ext);
		return (ext == ".rpgmvp" || ext == ".rpgmvm" || ext == ".rpgmvo");
	};

	Decrypter d{projectParams_, system_, encryptionKeyName_};

	for (const auto& p: fs::recursive_directory_iterator(path_)) {
		if (!isEncrypted(p)) { continue; }
		fs::path dst = outputDir / fs::relative(p, path_);
		fs::create_directories(dst.parent_path());
		d.decryptFile(p, dst);
	}
}
