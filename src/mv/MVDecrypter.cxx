#include "MVDecrypter.hxx"

#include <cstdio>
#include <fstream>
#include <locale>
#include <memory>
#include <stdexcept>
#include <string_view>

namespace fs = std::filesystem;

namespace {

	std::string realExtByFakeExt(std::string fakeExt) {
		std::use_facet<std::ctype<char>>(std::locale()).tolower(fakeExt.data(), fakeExt.data() + fakeExt.size());
		if (fakeExt == ".rpgmvp") {
			return "png";
		} else if (fakeExt == ".rpgmvm") {
			return "m4a";
		} else if (fakeExt == ".rpgmvo") {
			return "ogg";
		} else {
			return "unknown";
		}
	}

	void close_file(std::FILE* fp) {
		std::fclose(fp);
	}

	std::string readAll(const fs::path& fileName) {
		std::unique_ptr<std::FILE, decltype(&close_file)> fp(std::fopen(fileName.c_str(), "rb"), &close_file);
		if (fp) {
			std::string contents;
			contents.resize(fs::file_size(fileName));
			const auto contentSize = std::fread(&contents[0], 1, contents.size(), fp.get());
			if (contentSize != contents.size()) {
				throw std::runtime_error("Can not read from file " + fileName.string());
			}
			return contents;
		}
		throw std::runtime_error("Can not open file " + fileName.string());
	}

	std::string readDecryptionCode(const fs::path& filename, const std::string& keyName) {
		// example: "encryptionKey":"762125f4eea35aa39a0af0a29a472341"
		// a full JSON parsing is an overkill here, but I have to make this code more error-proof
		std::string content = readAll(filename);
		const auto i = content.find('"' + keyName + '"');
		if (i == content.npos) { throw std::runtime_error("Can not find key in the system file"); }

		const auto keySt = content.find("\"", i + keyName.size() + 2) + 1;
		const auto keyEnd = content.find("\"", keySt + 1);

		return content.substr(keySt, keyEnd - keySt);
	}

	std::vector<unsigned char> decryptionStrToBytes(const std::string& str) {
		if (str.empty()) { throw std::runtime_error("decryption key is empty"); }
		if (str.size() % 2 == 1) { throw std::runtime_error("decryption key length has to be even"); }

		std::vector<unsigned char> res(str.size() / 2);
		char* endp;
		for (std::size_t i = 0; i < str.size() / 2; ++i) { res[i] = ::strtol(str.substr(i * 2, 2).c_str(), &endp, 16); }

		return res;
	}
} // namespace

MV::Decrypter::Decrypter(LoadingParameters params, const std::string& decryptCode)
    : params_{std::move(params)}
    , decryptCode_{decryptionStrToBytes(decryptCode)}
    , decryptCodeStr_{decryptCode} {
}

MV::Decrypter::Decrypter(LoadingParameters params, const std::filesystem::path& filename, const std::string& keyName)
    : MV::Decrypter(std::move(params), readDecryptionCode(filename, keyName)) {
}

void MV::Decrypter::decryptFile(const std::filesystem::path& src, const std::filesystem::path& dest) const {
	if (decryptCode_.empty()) { throw std::logic_error("Decryption-Code is not set!"); }
	std::string fileContent = readAll(src);

	// Check if all required external stuff is here
	if (fileContent.empty()) throw std::runtime_error("File-Content is not loaded!");
	if (fileContent.length() < (params_.headerLength * 2)) {
		throw std::runtime_error("File is to short "); //(<" + (this.getHeaderLen() * 2) + " Bytes)");
	}

	// Check Header
	if (!params_.ignoreFakeHeader) {
		if (!isFakeHeaderValid(fileContent)) { throw std::runtime_error("Header is Invalid!"); }
	}

	// Remove Fake-Header from rest
	std::string_view content{fileContent.data() + params_.headerLength, fileContent.size() - params_.headerLength};

	unsigned char* ptr = reinterpret_cast<unsigned char*>(fileContent.data()) + params_.headerLength;
	// Decrypt Real-Header & First part of the Content
	if (content.length() > 0) {
		if (decryptCode_.size() != params_.headerLength) {
			throw std::runtime_error("Header lenght is not equal to decryption code length");
		}
		for (std::size_t i = 0; i < params_.headerLength; ++i) {
			ptr[i] = static_cast<unsigned char>(ptr[i]) ^ decryptCode_.at(i);
		}
	}

	fs::path res = dest;
	res.replace_extension(realExtByFakeExt(src.extension()));
	// Update File-Content
	std::ofstream out{res};
	out << content;
}

bool MV::Decrypter::isFakeHeaderValid(const std::string& content) const {
	std::string_view header{content.data(), params_.headerLength};
	std::vector<unsigned char> refBytes = std::vector<unsigned char>(params_.headerLength);
	std::string refStr = params_.signature + params_.version + params_.remain;

	// Generate reference bytes
	for (std::size_t i = 0; i < params_.headerLength; ++i) {
		std::size_t subStrStart = i * 2;
		char* endp;
		refBytes[i] = ::strtol(refStr.substr(subStrStart, 2).c_str(), &endp, 16);
	}

	// Verify header (Check if its an encrypted file)
	for (std::size_t i = 0; i < params_.headerLength; ++i) {
		if (refBytes[i] != header[i]) return false;
	}

	return true;
}

bool MV::Decrypter::isProjectEncrypted() const {
	return !decryptCode_.empty();
}
