#pragma once

#include "loadingParameters.hxx"

#include <filesystem>
#include <string>
#include <vector>

namespace MV {
	class Project {
	public:
		Project(const LoadingParameters& params, const std::filesystem::path& path, bool verify);

		void decrypt(const std::filesystem::path& outputDir);

	private:
		bool verifyProject();
		void ensureEncrypted();

		LoadingParameters projectParams_;
		std::filesystem::path path_;
		std::string encryptionKeyName_;
		bool isEncrypted_;
		std::filesystem::path system_;
		std::vector<std::filesystem::path> files_;
		std::vector<std::filesystem::path> encryptedFiles_;
	};
} // namespace MV
