#pragma once

#include "loadingParameters.hxx"

#include <filesystem>
#include <string>
#include <vector>

namespace MV {
	class Decrypter {
	public:
		Decrypter(LoadingParameters params, const std::string& decryptCode);
		Decrypter(LoadingParameters params, const std::filesystem::path& filename, const std::string& keyName);

		void decryptFile(const std::filesystem::path& src, const std::filesystem::path& dest) const;

		bool isProjectEncrypted() const;

	private:
		bool isFakeHeaderValid(const std::string& content) const;

		LoadingParameters params_;
		std::vector<unsigned char> decryptCode_;
		std::string decryptCodeStr_;
	};

} // namespace MV
