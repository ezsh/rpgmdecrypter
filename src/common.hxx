#pragma once

#include <filesystem>

namespace RPGM {

	enum class Version {
		Invalid, // Unsupported or undetectable version
		XP,
		VX,
		VXAce,
		MV
	};

	struct Project {
		Version version;
		std::filesystem::path path;
	};

	/**
	 * @brief Tries to guess RPGM version from the given path, that could be a file archive or MV project dir
	 */

	Project detectVersion(const std::filesystem::path& path);
} // namespace RPGM
