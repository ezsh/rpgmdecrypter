#include "common.hxx"
#include "mv/MVProject.hxx"
#include "xpvxace/RGSSArchive.hxx"

#include <iostream>

int main(int argc, char** argv) {
	if (argc != 3) {
		std::cerr << "Usage: " << argv[0] << " SRC DEST_DIR" << std::endl
		          << std::endl
		          << "SRC can be a RPGM XP or VX file (Game.rgss{3a,ad} or RPGM MV directory" << std::endl;
		return 2;
	}
	try {
		RPGM::Project p = RPGM::detectVersion(argv[1]);
		switch (p.version) {
			case ::RPGM::Version::MV: {
				MV::LoadingParameters params;
				MV::Project pr{params, p.path, true};
				pr.decrypt(argv[2]);
			} break;
			case RPGM::Version::XP:
			case RPGM::Version::VX: {
				RPGM::RGSSArchiveV1 archive{p.path};
				archive.extract(argv[2], true);
			} break;
			case RPGM::Version::VXAce: {
				RPGM::RGSSArchiveV3 archive{p.path};
				archive.extract(argv[2], true);
			} break;
			case RPGM::Version::Invalid:
				std::cerr << "Can't detect RPGM version for this path" << std::endl;
				return 2;
		}
		return 0;
	} catch (std::exception& ex) { std::cerr << "Error: " << ex.what() << std::endl; }
}
