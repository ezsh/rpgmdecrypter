#include "common.hxx"

#include <map>
#include <string>

namespace fs = std::filesystem;

namespace {
	struct KeyPath {
		RPGM::Version version;
		bool isDir;
		bool returnDir;
	};
	const std::map<fs::path, KeyPath> versionDetectionPaths = {
	    {"Game.rgssad", {RPGM::Version::XP, false, false}},
	    {"Game.rgss2a", {RPGM::Version::VX, false, false}},
	    {"Game.rgss3a", {RPGM::Version::VXAce, false, false}},
	    {"natives_blob.bin", {RPGM::Version::MV, false, true}},
	};
} // namespace

RPGM::Project RPGM::detectVersion(const std::filesystem::path& path) {
	if (fs::is_directory(path)) {
		for (const auto& test: versionDetectionPaths) {
			if ((test.second.isDir && fs::is_directory(path / test.first)) || fs::is_regular_file(path / test.first)) {
				return test.second.returnDir ? RPGM::Project{test.second.version, path} :
				                           RPGM::Project{test.second.version, path / test.first};
			}
		}
		return {RPGM::Version::Invalid, path};
	}

	const std::string fn = path.filename().string();
	for (const auto& test: versionDetectionPaths) {
		if (!test.second.isDir && fn == test.first) { return {test.second.version, path}; }
	}
	return {RPGM::Version::Invalid, path};
}
